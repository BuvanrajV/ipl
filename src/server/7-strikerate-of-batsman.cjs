function strikeRateOfBatsman(matches, deliveries) {
  let iDYear = matches.reduce((objId, match) => {
    objId[match.id] = match.season;
    return objId;
  }, {});
  let batsmansStrikerate = deliveries.reduce((objYear, delivery) => {
    let year = iDYear[delivery.match_id];
    let batsman = delivery.batsman;
    let runs = delivery.batsman_runs;
    if (objYear.hasOwnProperty(year)) {
      if (objYear[year].hasOwnProperty(batsman)) {
        objYear[year][batsman].totalRuns += parseInt(runs);
        objYear[year][batsman].totalBalls += 1;
        if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
          objYear[year][batsman].totalBalls -= 1;
        }
      } else {
        objYear[year][batsman] = {};
        objYear[year][batsman].totalRuns = parseInt(runs);
        objYear[year][batsman].totalBalls = 1;
        if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
          objYear[year][batsman].totalBalls -= 1;
        }
      }
    } else {
      objYear[year] = {};
      objYear[year][batsman] = {};
      objYear[year][batsman].totalRuns = parseInt(runs);
      objYear[year][batsman].totalBalls = 1;
      if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
        objYear[year][batsman].totalBalls -= 1;
      }
    }
    objYear[year][batsman].strikeRate = (
      (objYear[year][batsman].totalRuns / objYear[year][batsman].totalBalls) *
      100
    ).toFixed(2);
    return objYear;
  }, {});
  return batsmansStrikerate;
}
module.exports = strikeRateOfBatsman;
