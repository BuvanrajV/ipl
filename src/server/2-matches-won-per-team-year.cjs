function matchesWonPerYear(matches) {
  let WonPerYear = matches.reduce((objYear, match) => {
    let year = match.season;
    let team = match.winner;
    if (objYear.hasOwnProperty(year)) {
      if (objYear[year].hasOwnProperty(team)) {
        objYear[year][team] += 1;
      } else {
        objYear[year][team] = 1;
      }
    } else {
      objYear[year] = {};
      objYear[year][team] = 1;
    }
    return objYear;
  }, {});
  return WonPerYear;
}

module.exports = matchesWonPerYear;
