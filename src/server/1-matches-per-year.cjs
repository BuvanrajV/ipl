function matchesPerYear(matches) {
  let matchCount = matches.reduce(function (objYear, match) {
    let year = match.season;
    if (objYear.hasOwnProperty(year)) {
      objYear[year] += 1;
    } else {
      objYear[year] = 1;
    }
    return objYear;
  }, {});
  return matchCount;
}

module.exports = matchesPerYear;
