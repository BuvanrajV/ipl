function highestManOfMatch(matches) {
  let manOfMatch = matches.reduce((objYear, match) => {
    let player = match.player_of_match;
    let year = match.season;
    if (objYear.hasOwnProperty(year)) {
      if (objYear[year].hasOwnProperty(player)) {
        objYear[year][player] += 1;
      } else {
        objYear[year][player] = 1;
      }
    } else {
      objYear[year] = {};
      objYear[year][player] = 1;
    }
    return objYear;
  }, {});
  let maxManOfMatch = Object.keys(manOfMatch).reduce((accu, year) => {
    let players = Object.keys(manOfMatch[year]).sort(
      (player1, player2) =>
        manOfMatch[year][player2] - manOfMatch[year][player1]
    );
    accu[year] = players[0];
    return accu;
  }, {});
  return maxManOfMatch;
}
module.exports = highestManOfMatch;
