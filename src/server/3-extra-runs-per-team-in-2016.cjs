function extraRunsPerTeam(matches, deliveries, year) {
  let id = matches
    .filter((match) => match.season == year)
    .map((match) => match.id);
  let extraRuns = deliveries.reduce(function (objTeam, delivery) {
    let team = delivery.bowling_team;
    let extras = delivery.extra_runs;
    if (id.includes(delivery.match_id) && extras !== "0") {
      if (objTeam.hasOwnProperty(team)) {
        objTeam[team] += parseInt(extras);
      } else {
        objTeam[team] = 1;
      }
    }
    return objTeam;
  }, {});
  return extraRuns;
}

module.exports = extraRunsPerTeam;
