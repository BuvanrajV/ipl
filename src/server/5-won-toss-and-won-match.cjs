function wonTossWonMatch(matches) {
  let wonTeams = matches.reduce((objWinner, match) => {
    if (match.toss_winner === match.winner) {
      if (objWinner.hasOwnProperty(match.winner)) {
        objWinner[match.winner] += 1;
      } else {
        objWinner[match.winner] = 1;
      }
    }
    return objWinner;
  }, {});
  return wonTeams;
}

module.exports = wonTossWonMatch;