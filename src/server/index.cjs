// Package for convert file csv to json
const csv = require("csvtojson");

// File System Module
const fs = require("fs");

// Path directory Module
const Path = require("path");

// Path for matches.csv and deliveries.csv
const csvFileMatchesPath = Path.join(__dirname, "../data/matches.csv");
const csvFileDeliveriesPath = Path.join(__dirname, "../data/deliveries.csv");

// require the functions
const matchesPerYear = require(Path.join(__dirname, "1-matches-per-year.cjs"));
const matchesWonPerYear = require(Path.join(
  __dirname,
  "2-matches-won-per-team-year.cjs"
));
const extraRunsPerTeam = require(Path.join(
  __dirname,
  "3-extra-runs-per-team-in-2016.cjs"
));
const economicalBowlers = require(Path.join(
  __dirname,
  "4-top10-economical-bowlers.cjs"
));
const wonTossWonMatch = require(Path.join(
  __dirname,
  "5-won-toss-and-won-match.cjs"
));
const highestManOfMatch = require(Path.join(
  __dirname,
  "6-highest-man-of-match.cjs"
));
const strikeRateOfBatsman = require(Path.join(
  __dirname,
  "./7-strikerate-of-batsman.cjs"
));
const maximumDismissedPlayers = require(Path.join(
  __dirname,
  "8-highest-one-player-dismissal-by-another-player.cjs"
));
const bestEconomy = require(Path.join(
  __dirname,
  "./9-best-economy-in-super-over.cjs"
));

// Using CSV package call the function and write the output into Json file
csv()
  .fromFile(csvFileDeliveriesPath)
  .then((deliveries) => {
    csv()
      .fromFile(csvFileMatchesPath)
      .then((iplMatches) => {
        // Problem-1 : Matches Per Year
        let result1 = matchesPerYear(iplMatches);
        const jsonFilePath1 = Path.join(
          __dirname,
          "../public/output/matchesPerYear.json"
        );
        fs.writeFileSync(jsonFilePath1, JSON.stringify(result1));
        // Problem-2 : Matches won per Team Per Year
        let result2 = matchesWonPerYear(iplMatches);
        const jsonFilePath2 = Path.join(
          __dirname,
          "../public/output/matchesWonperteamsPeryears.json"
        );
        fs.writeFileSync(jsonFilePath2, JSON.stringify(result2));
        // Problem-3 : Extra runs per team in 2016
        let result3 = extraRunsPerTeam(iplMatches, deliveries, 2016);
        const jsonFilePath3 = Path.join(
          __dirname,
          "../public/output/extraRunsPerTeamin2016.json"
        );
        fs.writeFileSync(jsonFilePath3, JSON.stringify(result3));
        // Problem-4 : Top 10 economical Bowlers
        let result4 = economicalBowlers(iplMatches, deliveries, "2015");
        const jsonFilePath4 = Path.join(
          __dirname,
          "../public/output/top10EconomicalBowlers.json"
        );
        fs.writeFileSync(jsonFilePath4, JSON.stringify(result4));
        // Problem-5 : Each team won the toss and also won the match
        let result5 = wonTossWonMatch(iplMatches);
        const jsonFilePath5 = Path.join(
          __dirname,
          "../public/output/wonTheTossAndMatch.json"
        );
        fs.writeFileSync(jsonFilePath5, JSON.stringify(result5));
        // Problem-6 : Highest man of matches from each year
        let result6 = highestManOfMatch(iplMatches);
        const jsonFilePath6 = Path.join(
          __dirname,
          "../public/output/highestManOfMatch.json"
        );
        fs.writeFileSync(jsonFilePath6, JSON.stringify(result6));
        // Problem-7 : Strikerate of batsmans per year
        let result7 = strikeRateOfBatsman(iplMatches, deliveries);
        const jsonFilePath7 = Path.join(
          __dirname,
          "../public/output/strikeRateOfBatsman.json"
        );
        fs.writeFileSync(jsonFilePath7, JSON.stringify(result7));
        // Problem-8 : Highest on player dismissal by another player
        let result8 = maximumDismissedPlayers(deliveries);
        const jsonFilePath8 = Path.join(
          __dirname,
          "../public/output/maximumDismissalbyOnePlayer.json"
        );
        fs.writeFileSync(jsonFilePath8, JSON.stringify(result8));
        //Problem-9 : Best Economy in super-over
        let result9 = bestEconomy(deliveries);
        const jsonFilePath9 = Path.join(
          __dirname,
          "../public/output/bestEconomyInSuperOver.json"
        );
        fs.writeFileSync(jsonFilePath9, JSON.stringify(result9));
      });
  });
