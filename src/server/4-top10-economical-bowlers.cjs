function economicalBowlers(matches, deliveries, year) {
  // id for 2015
  let id2015 = matches
    .filter((match) => match.season === year)
    .map((match) => match.id);
  // deliveries for 2015
  let delivery2015 = deliveries.filter((delivery) => {
    if (id2015.includes(delivery.match_id)) {
      return delivery;
    }
  });
  // Creating Object for economic bowlers
  let objEconomicBowlers = delivery2015.reduce((objBowler, delivery) => {
    let bowler = delivery.bowler;
    if (objBowler.hasOwnProperty(bowler)) {
      objBowler[bowler]["totalRuns"] += parseInt(delivery.total_runs);
      objBowler[bowler]["totalBalls"] += 1;
      if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
        objBowler[bowler]["totalBalls"] -= 1;
      }
      // Economic of bowler
      objBowler[bowler]["Economic"] = (
        objBowler[bowler]["totalRuns"] /
        (objBowler[bowler]["totalBalls"] / 6)
      ).toFixed(2);
    } else {
      objBowler[bowler] = {};
      objBowler[bowler]["totalRuns"] = parseInt(delivery.total_runs);
      objBowler[bowler]["totalBalls"] = 1;
      if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
        objBowler[bowler]["totalBalls"] -= 1;
      }
    }
    return objBowler;
  }, {});
  // sorting economics
  let arrayEconomicBowlers = Object.fromEntries(
    Object.entries(objEconomicBowlers)
      .sort((bowler1, bowler2) => {
        return bowler1[1].Economic - bowler2[1].Economic;
      })
      .slice(0, 10)
  );
  return arrayEconomicBowlers;
}
module.exports = economicalBowlers;
