function bestEconomy(deliveries) {
  let superOver = deliveries.filter(
    (delivery) => delivery.is_super_over !== "0"
  );
  let bestEconomyInSuperOver = superOver.reduce((objBowler, delivery) => {
    let bowler = delivery.bowler;
    let runs = delivery.total_runs;
    if (objBowler.hasOwnProperty(bowler)) {
      objBowler[bowler].TotalRuns += parseInt(runs);
      objBowler[bowler].TotalBall += 1;
      if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
        objBowler[bowler].TotalBall -= 1;
      }
    } else {
      objBowler[bowler] = {};
      objBowler[bowler].TotalRuns = parseInt(runs);
      objBowler[bowler].TotalBall = 1;
      if (delivery.wide_runs !== "0" || delivery.noball_runs !== "0") {
        objBowler[bowler].TotalBall -= 1;
      }
    }
    objBowler[bowler].Economy = (
      objBowler[bowler].TotalRuns /
      (objBowler[bowler].TotalBall / 6)
    ).toFixed(2);
    return objBowler;
  }, {});
  let arrBestBowler = Object.fromEntries(
    Object.entries(bestEconomyInSuperOver)
      .sort((bowler1, bowler2) => bowler1[1].Economy - bowler2[1].Economy)
      .slice(0, 1)
  );
  return arrBestBowler;
}

module.exports = bestEconomy;
